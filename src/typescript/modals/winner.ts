import { Fighter } from "../models/fighter";
import { createElement } from '../helpers/domHelper'; 
import { showModal } from './modal'

export  function showWinnerModal(fighter: Fighter) {
  // show winner name and image
  const { name, source } = fighter;

  const  winnerContainer = createElement({ tagName: 'div', className: 'winner-container' })

  const winnerPic = createElement({ tagName: 'img', className: 'winner-pic', attributes: {src:source} });
  winnerContainer.append(winnerPic);
  const header: string = 'This is the winner: '+fighter.name;
  showModal({title: header,bodyElement: winnerContainer});
}
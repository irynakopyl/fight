import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { Fighter } from '../models/fighter'
 //відображення детальної інформації про бійця
export  function showFighterDetailsModal(fighter:Fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter:Fighter) {
  const { name } = fighter;
  const { attack } = fighter;
  const { defense } = fighter;
  const { health } = fighter;
  const { source } = fighter;


  const fighterDetails = createElement({ tagName: 'div', className: 'fighter' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  
  // show fighter name, attack, defense, health, image
  const fighterAttackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
  const fighterDefenceElement = createElement({ tagName: 'span', className: 'fighter-defence' });
  const fighterHealthElement = createElement({ tagName: 'span', className: 'fighter-health' });
  const fighterImageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: {src: source} });


  nameElement.innerText = name;
  fighterDetails.append(nameElement);

  fighterAttackElement.innerText = 'Attack: ' + attack!.toString();
  fighterDetails.append(fighterAttackElement);

  fighterDefenceElement.innerText = 'Defense: ' + defense!.toString();
  fighterDetails.append(fighterDefenceElement);

  fighterHealthElement.innerText = 'Health: '+ health!.toString();
  fighterDetails.append(fighterHealthElement);

  fighterDetails.append(fighterImageElement);

  return fighterDetails;
}

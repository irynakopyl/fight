import { Fighter } from './models/fighter'
export async function fight(firstFighter: Fighter, secondFighter: Fighter):Promise<Fighter> {
  // return winner
  console.log('THE GAME STARTED!');
 return new Promise(function(resolve, reject) {
   while(true)
      {
        const damage = getDamage(firstFighter, secondFighter);
        secondFighter.health!  -= (damage > 0) ? damage : 0 ;
        if (secondFighter.health! <= 0) {
          resolve(firstFighter);
          break;
        }
        console.log(secondFighter.name +' Health: ' + secondFighter.health);

        const damage1 = getDamage(secondFighter, firstFighter);
        firstFighter.health!  -= (damage1 > 0) ? damage1 : 0
        if (firstFighter.health! <= 0) {
          resolve(secondFighter);
          break;
        }
        console.log(firstFighter.name + ' Health: ' + firstFighter.health);
      }
    });
}

export function getDamage(attacker: Fighter, enemy: Fighter) {
  // damage = hit - block
  const damage: number = getHitPower(attacker)-getBlockPower(enemy);
  return damage;
}

export function getHitPower(fighter: Fighter) {
  // return hit power
  const criticalHitChance : number = Math.random() + 1;
  const power: number= fighter!.attack! * criticalHitChance;
  return power;
}

export function getBlockPower(fighter: Fighter) {
  // return block power
  const dodgeChance : number = Math.random() + 1;
  const power: number= fighter!.defense! * dodgeChance;
  return power;
}

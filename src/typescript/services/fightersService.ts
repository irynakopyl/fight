import { callApi } from '../helpers/apiHelper';
import { Fighter } from '../models/fighter';

export async function getFighters() {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');
    
    return <Fighter[]>apiResult;
  } catch (error) {
    throw error;
  }
}
export async function getFighterDetails(id:string) {
  // endpoint - `details/fighter/${id}.json`;
  try {
    const endpoint: string = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET');
    
    return <Fighter>apiResult;
  } catch (error) {
    throw error;
  }
}


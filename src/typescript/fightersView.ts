import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { getFighterDetails } from './services/fightersService';
import { Fighter } from './models/fighter';

export function createFighters(fighters:Fighter[] ) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map();

//опрацювання кліка по бійцю 
async function showFighterDetails(event:MouseEvent, fighter: Fighter) {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId:string) {
  // get fighter form fightersDetailsCache or use getFighterDetails function
  try {
    const fighter = await getFighterDetails(fighterId);
    return fighter;
  }
  catch(error){
    throw error;
  }
  
}

function createFightersSelector() {
  const selectedFighters = new Map();

  return async function selectFighterForBattle(event:Event, fighter: Fighter) {
    const fullInfo = await getFighterInfo(fighter._id);
    const checkbox = event.target as HTMLInputElement;

    if (checkbox.checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const iterator = selectedFighters.values();
      const firstPl : Fighter = iterator.next().value as Fighter;
      const secondPl : Fighter = iterator.next().value as Fighter;
      const firstPlHelth = firstPl.health;
      const secondPlHelth = secondPl.health;

      const winner: Fighter= await fight(firstPl, secondPl);
      firstPl.health = firstPlHelth;
      secondPl.health = secondPlHelth;
      showWinnerModal(winner);
      
    }
  }
}
